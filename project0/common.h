#pragma once

#include <mutex>
#include <cctype>
#include <memory>
#include <thread>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <unordered_map>

using Map = std::unordered_map<std::string, int>;

enum ArgCount{ MIN = 3, MAX  = 6};
enum Return{ SUCCESS = 0, INCORRECT_ARGS, OPEN_FAILURE};
