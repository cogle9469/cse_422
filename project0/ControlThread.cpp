#include "ControlThread.h"

ControlThread::ControlThread(std::vector<std::string> &query, std::ifstream & f) : file(f)
{
   std::for_each(query.begin(),query.end(),[this](std::string &s)
   {
      word_count[s] = 0; 
   });
   thread_count = std::thread::hardware_concurrency();
   if(thread_count == 0){thread_count = 1;}
   for(int i =0; i < thread_count; i++ )
   {
      thread_vec.push_back(std::make_shared<CounterThread>(query,word_count));
   }
}



ControlThread::~ControlThread()
{
   //std::cout << "Killing Control Thread" << std::endl;
}

void ControlThread::startSearch()
{
   std::for_each(thread_vec.begin(), thread_vec.end(),[](std::shared_ptr<CounterThread> ptr)
   {
      ptr->launchThread();
   });

   unsigned int current_thread = 0;
   std::string line;
   while(std::getline(file,line))
   {
      thread_vec[current_thread]->pushToQueue(line);
      current_thread = (current_thread + 1) % thread_count; 
   }

   std::for_each(thread_vec.begin(), thread_vec.end(),[](std::shared_ptr<CounterThread> ptr)
   {
      ptr->signalShutdown();
      ptr->joinThread();
   });
}

void ControlThread::printToFile(std::ofstream & ofs)
{
   std::for_each(word_count.begin(), word_count.end(),[&] (std::pair<std::string, int>  p)
   {
     ofs << p.first  << ", " << p.second << "\n";
   });
}

void ControlThread::printToScreen()
{
   std::for_each(word_count.begin(), word_count.end(),[] (std::pair<std::string, int>  p)
   {
     std::cout << p.first  << ", " << p.second << std::endl;
   });
}
