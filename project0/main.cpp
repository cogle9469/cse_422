
#include "common.h"
#include <cstring>
#include "ControlThread.h"

void printHelp()
{
   std::cout << "Usage: query_word <search_file>" << std::endl;
   std::cout << "Usage: -f <query_file> -w <result_output> <search_file>" << std::endl;
}

int main(int argc, char *argv[])
{
   int out_flag = 0;
   int query_flag = 0;
   int read_file_flag = 0;
   std::ofstream out_stream; 
   std::ifstream search_stream; 

   std::vector<std::string> search_words;
   if( argc <  ArgCount::MIN || argc > ArgCount::MAX)
   {
      printHelp();
      return Return::INCORRECT_ARGS;
   }
   else if( argc == ArgCount::MIN)
   {
      std::vector<std::string> search_word {argv[1]};
      std::ifstream stream(argv[2], std::ifstream::in); 
      if(stream.is_open())
      {
         ControlThread c_t {search_word, stream};
         c_t.startSearch();
         c_t.printToScreen();
         return Return::SUCCESS;
      }
      std::cout << "Can not open provided file: " << argv[2] << std::endl;
      return Return::OPEN_FAILURE;
   }
   else
   {
      int args_limit = argc - 1;
      int i = 1; 
      while(i < args_limit)
      {
         if(strcmp(argv[i], "-f") == 0)
         {
            if(i + 1 == args_limit || query_flag == 1)
            {
               printHelp();
               return Return::INCORRECT_ARGS;
            }
            query_flag = 1;
            std::ifstream query_stream; 
            query_stream.open(argv[i + 1], std::ifstream::in); 
            if(!query_stream.is_open())
            {
               std::cout << "Can not open option -f provided file: " << argv[i+1] << std::endl;
               return Return::OPEN_FAILURE; 
            }
            std::string line;
            while (std::getline(query_stream, line))
            {
               search_words.push_back(line);          
            }
            i+=2;
         }
         else if(strcmp(argv[i], "-w") == 0)
         {
            if(i + 1 == args_limit || out_flag  == 1)
            {
               printHelp();
               return Return::INCORRECT_ARGS;
            }
            out_flag = 1;
            out_stream.open(argv[i+1], std::ofstream::out);
            if(!out_stream.is_open())
            {
               std::cout << "Can not open option -w provided file: " << argv[i+1] << std::endl;
               return Return::OPEN_FAILURE; 
            }
            i+=2;
         }
         else
         {
            if(query_flag == 1)
            {
               printHelp();
               return Return::INCORRECT_ARGS;
            }
            else
            {
               query_flag = 1;
               search_words.push_back(argv[i]);  
            }
            i++; 
         }
      }

      search_stream.open(argv[args_limit], std::ifstream::in); 
      if(!search_stream.is_open())
      {
         std::cout << "Can not open provided file: " << argv[i] << std::endl;
         return Return::OPEN_FAILURE; 
      } 

      ControlThread c_t {search_words, search_stream};
      c_t.startSearch();
      if(out_flag == 0){c_t.printToScreen();}
      else{c_t.printToFile(out_stream);}
      return Return::SUCCESS;
   }
}
