#pragma once

#include <deque>

#include "common.h"
#include "t_deque.h"
#include "spinlock.h"

extern spinlock_mutex map_mutex;


class CounterThread
{
public:

   CounterThread(std::vector<std::string> & q_w, Map & w_c);
   ~CounterThread();
   
   void proccess();
   void joinThread();
   void launchThread();
   void signalShutdown();

   void pushToQueue(std::string & s);

private:

   bool running;
   std::thread t;
   t_deque<std::string> queue; 
   std::vector<std::string> & query_words;
   std::unordered_map<std::string, int> & word_count_ref;

   spinlock_mutex deque_mutex;
};
