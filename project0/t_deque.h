#pragma once

#include "deque"
#include "mutex"
#include "memory"
#include "spinlock.h"

template <typename T>
class t_deque
{
public:
   t_deque(){}
   ~t_deque(){ deq.clear(); }

   std::shared_ptr<T> pop()
   {
      std::lock_guard<spinlock_mutex> lock(s_m);
      if(deq.size() > 0)
      {
         std::shared_ptr<T> ret = std::make_shared<T>(deq.front());
         deq.pop_front();
         return ret;
      }
      return nullptr;
   }

   void push(T ele)
   {
      std::lock_guard<spinlock_mutex> lock(s_m);
      deq.push_back(ele);
   }
   
private:
   
   std::deque<T> deq;
   spinlock_mutex s_m;   
};

