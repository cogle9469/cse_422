#pragma once

#include "common.h"
#include "CounterThread.h"

class ControlThread
{
public:

   ControlThread(std::vector<std::string> & q, std::ifstream & f);
   ~ControlThread();

   void startSearch();
   void printToFile(std::ofstream & ofs);
   void printToScreen();


private:

   std::ifstream & file; 
   Map  word_count;
   std::vector<std::shared_ptr<CounterThread>> thread_vec;

   int thread_count;
};
