#include "CounterThread.h"

spinlock_mutex map_mutex {};

CounterThread::CounterThread(std::vector<std::string> & q_w, Map & w_c) : query_words(q_w),
                                                                          word_count_ref(w_c)
{
   running = true;
}

CounterThread::~CounterThread()
{
   //std::cout << "Killing thread" << std::endl;
}


void CounterThread::joinThread()
{
   if(t.joinable())
   {
      t.join();
   }
}

void CounterThread::launchThread()
{
   std::thread launch([this]
   {
      proccess();
   });

   t = std::move(launch); 
}

void CounterThread::proccess()
{
   while(running)
   {
      std::shared_ptr<std::string> ele = queue.pop();
      if(ele != nullptr)
      {
         std::string line = *(ele);
         if(line.compare("0_TERMINATE_THREAD") == 0)
         {
            running = false;
            break;
         }
         //Actual Line Searching Algorithm
         std::stringstream ss(line);
         std::istream_iterator<std::string> begin(ss);
         std::istream_iterator<std::string> end;
         std::vector<std::string> vstrings(begin, end); 
         // std::for_each(query_words.begin(), query_words.end(),[&] (std::string &query_word)
         std::for_each(vstrings.begin(),vstrings.end(),[&] (std::string & s)
         {
            // std::for_each(vstrings.begin(),vstrings.end(),[&] (std::string & s)
            std::for_each(query_words.begin(), query_words.end(),[&] (std::string &query_word)
            {
               if(s.size() < query_word.size()){ return;}
               std::size_t pos = s.find(query_word);
               if(pos == std::string::npos)
               {
                  return;
               }
               //Check before
               for(std::size_t iter = 0; iter < pos; iter++)
               {
                  if(!ispunct(s[iter]))
                  {
                     return;
                  }
               } 
               //Check after
               for(std::size_t iter = pos + query_word.size(); iter < s.size(); iter++)
               {
                  if(!ispunct(s[iter]))
                  {
                     return;
                  }
               } 
               {
                  std::lock_guard<spinlock_mutex> lock(map_mutex);
                  word_count_ref[query_word] += 1;
               }
            });
         });
      }
   }
}
   
void CounterThread::pushToQueue(std::string & s)
{
   std::string temp = std::move(s);
   queue.push(temp);
}
   
void CounterThread::signalShutdown()
{
   queue.push("0_TERMINATE_THREAD");
}

